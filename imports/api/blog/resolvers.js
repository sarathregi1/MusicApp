import Blogs from "./Blog";


export default {
    Query: {
        blog(obj, args, { userId, content }) {
            return Blogs.find({
                userId,
                content
            }).fetch();
        }
    },
    
    Mutation: {
        createBlog(obj, { content }, { userId }) {
            const blogId = Blogs.insert({
                content,
                userId
            });
            return Blogs.findOne(blogId);
        },
        removeBlog(obj, { id }) {
            return Blogs.remove({
                _id: id
            });
        }
    }
};