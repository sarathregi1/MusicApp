import React, { Component } from 'react';
import { render } from "react-dom";
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import searchParams from 'url';

const styles = {
  card: {
    width: 80 + "vw",
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    marginBottom: 16,
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
};

class AccountsUI extends Component {
    constructor(props) {
        super(props);
    }

    onClick () {
        const value = new URL(window.location.href)
        console.log(value)
        value.searchParams.forEach((value, name) => {
           if (name === 'code') {
                Meteor.call('OauthGet',value)
           } 
           
        });
    }
    render() {
        const { classes } = this.props;
        return (
            <div >
                <div style={{padding: 20, paddingTop: 40}}>
                    <Card className={classes.card} onLoad={this.onclick}>
                        <CardContent>
                            <Typography variant="headline" component="h2">
                                Link your Patreon account
                            </Typography>
                            <br/>
                            <Button variant="raised" onClick={this.onClick} className={classes.button} href="/PatreonAuth">
                                Link Patreon
                            </Button>
                        </CardContent>
                        <CardActions>
                            <Button size="small" onClick={this.onClick}>Learn More</Button>
                        </CardActions>
                    </Card>
                </div>
                <div style={{padding: 20}}>
                    <Card className={classes.card}>
                        <CardContent>
                        <Typography variant="headline" component="h2">
                            Email Subscriptions
                        </Typography>
                        </CardContent>
                        <CardActions>
                        <Button size="small">Learn More</Button>
                        </CardActions>
                    </Card>
                </div>
            </div>
        )
    }
}

AccountsUI.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AccountsUI);