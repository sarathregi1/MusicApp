import React from 'react';
import { render } from 'react-dom';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom'
import HomePage from './Pages/Home';
import BlogPage from './Pages/Blog';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { withApollo } from 'react-apollo';
import AccountPage from '../ui/Pages/Accounts';
import Admin from '../ui/Pages/Admin';
import BlogEditor from './BlogUI/BlogEditor';

const App = ({ loading, user, client }) => {
    if (loading) return null;
    return (
        <div>
            <Router>
                <div>
                    <Route exact path="/" component={HomePage}/>
                    <Route exact path="/Blog" component={BlogPage}/>
                    <Route exact path="/Account" component={AccountPage}/>
                    <Route exact path="/Account/:reidrect" component={AccountPage}/>
                    <Route exact path="/Admin" component={Admin}/>
                    <Route path='/PatreonAuth' component={() => window.location = 'https://www.patreon.com/oauth2/authorize?response_type=code&client_id=L0XIO1Oi3NFjIft0Ceer8e8MyEa_7HWFI2aLd6P-HiTPKbXdC_DbfYITJ3Tz60Nb&redirect_uri=http://localhost:3000/Account/redirect&scope=users pledges-to-me my-campaign'}/>
                    <Route exact path="/BlogEditor" component={BlogEditor}/>
                </div>
            </Router>
        </div>
    );
};

const UserQuery = gql `
    query Users {
        user {
            _id
        }
    }
`;

export default graphql(UserQuery, {
    props: ({data}) =>({ ... data })
  })(withApollo(App));