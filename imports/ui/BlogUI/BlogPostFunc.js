import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import Button from 'material-ui/Button';


const createBlog = gql`
    mutation createBlog($content: String!) {
        createBlog(content: $content) {
            _id
            content
        }
    }
`;

class BlogForm extends Component {
    
    submitForm = () => {
        console.log(this.content.value);
        this.props
            .createBlog({
                variables:{
                    content: this.content.value
                }
            })
            // .then(({ data }) => {
            // }
            .catch((error => {
                console.log(error);
            }));
       
    };
    
    render() {
        return (
            <div>
                <form>
                    <input type="text" ref={(input) => this.content = input} />
                    <Button variant="raised" color="primary" onClick={this.submitForm}>
                        Hello World
                    </Button>
                </form>
            </div>
        )
    }
}

export default graphql(createBlog, {
    name: "createBlog",
    options: {
        refetchQueries: [
            'blog'
        ]
    }
})(BlogForm);