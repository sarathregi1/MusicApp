import React, { Component } from 'react';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import './EditorCss.css';
import MediaQuery from 'react-responsive';
import SimpleAppBar from '../NavigationBar';
import SimpleAppBarMobile from '../NavigationBarMobile';
import Flexbox from 'flexbox-react';
import Grid from 'material-ui/Grid';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent } from 'material-ui/Card'
import Button from 'material-ui/Button';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import SaveIcon from 'material-ui-icons/Save';
import Bold from 'material-ui-icons/FormatBold';
import Italic from 'material-ui-icons/FormatItalic';
import Link from 'material-ui-icons/Link';
import UnOrderedList from 'material-ui-icons/FormatListBulleted';
import OrderedList from 'material-ui-icons/FormatListNumbered';
import BlockQuote from 'material-ui-icons/FormatQuote';
import Underline from 'material-ui-icons/FormatUnderlined';
import Title from 'material-ui-icons/Title';
import strikethrough from 'material-ui-icons/StrikethroughS';
import DeleteIcon from 'material-ui-icons/Delete';
import PublicIcon from 'material-ui-icons/Public';


const createBlog = gql`
    mutation createBlog($content: String!) {
        createBlog(content: $content) {
            _id
            content
        }
    }
`;

// const actions = {
//   options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link', 'embedded', 'emoji', 'remove', 'history'],
//   inline: {
//     inDropdown: false,
//     className: undefined,
//     component: undefined,
//     dropdownClassName: undefined,
//     options: ['bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript', 'subscript'],
//     bold: { icon: Bold, className: undefined },
//     italic: { icon: Italic, className: undefined },
//     underline: { icon: Underline, className: undefined },
//     strikethrough: { icon: strikethrough, className: undefined },
//     //monospace: { icon: monospace, className: undefined },
//     //superscript: { icon: superscript, className: undefined },
//     //subscript: { icon: subscript, className: undefined },
//   },
// //   blockType: {
// //     inDropdown: true,
// //     options: ['Normal', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'Blockquote', 'Code'],
// //     className: undefined,
// //     component: undefined,
// //     dropdownClassName: undefined,
// //   },
// //   fontSize: {
// //     //icon: fontSize,
// //     options: [8, 9, 10, 11, 12, 14, 16, 18, 24, 30, 36, 48, 60, 72, 96],
// //     className: undefined,
// //     component: undefined,
// //     dropdownClassName: undefined,
// //   },
// //   fontFamily: {
// //     options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana'],
// //     className: undefined,
// //     component: undefined,
// //     dropdownClassName: undefined,
// //   },
// //   list: {
// //     inDropdown: false,
// //     className: undefined,
// //     component: undefined,
// //     dropdownClassName: undefined,
// //     options: ['unordered', 'ordered', 'indent', 'outdent'],
// //     unordered: { icon: UnOrderedList, className: undefined },
// //     ordered: { icon: OrderedList, className: undefined },
// //     indent: { icon: indent, className: undefined },
// //     outdent: { icon: outdent, className: undefined },
// //   },
// //   textAlign: {
// //     inDropdown: false,
// //     className: undefined,
// //     component: undefined,
// //     dropdownClassName: undefined,
// //     options: ['left', 'center', 'right', 'justify'],
// //     left: { icon: left, className: undefined },
// //     center: { icon: center, className: undefined },
// //     right: { icon: right, className: undefined },
// //     justify: { icon: justify, className: undefined },
// //   },
// //   colorPicker: {
// //     icon: color,
// //     className: undefined,
// //     component: undefined,
// //     popupClassName: undefined,
// //     colors: ['rgb(97,189,109)', 'rgb(26,188,156)', 'rgb(84,172,210)', 'rgb(44,130,201)',
// //       'rgb(147,101,184)', 'rgb(71,85,119)', 'rgb(204,204,204)', 'rgb(65,168,95)', 'rgb(0,168,133)',
// //       'rgb(61,142,185)', 'rgb(41,105,176)', 'rgb(85,57,130)', 'rgb(40,50,78)', 'rgb(0,0,0)',
// //       'rgb(247,218,100)', 'rgb(251,160,38)', 'rgb(235,107,86)', 'rgb(226,80,65)', 'rgb(163,143,132)',
// //       'rgb(239,239,239)', 'rgb(255,255,255)', 'rgb(250,197,28)', 'rgb(243,121,52)', 'rgb(209,72,65)',
// //       'rgb(184,49,47)', 'rgb(124,112,107)', 'rgb(209,213,216)'],
// //   },
// //   link: {
// //     inDropdown: false,
// //     className: undefined,
// //     component: undefined,
// //     popupClassName: undefined,
// //     dropdownClassName: undefined,
// //     showOpenOptionOnHover: true,
// //     defaultTargetOption: '_self',
// //     options: ['link', 'unlink'],
// //     link: { icon: link, className: undefined },
// //     unlink: { icon: unlink, className: undefined },
// //   },
// //   emoji: {
// //     //icon: emoji,
// //     className: undefined,
// //     component: undefined,
// //     popupClassName: undefined,
// //     emojis: [
// //       '😀', '😁', '😂', '😃', '😉', '😋', '😎', '😍', '😗', '🤗', '🤔', '😣', '😫', '😴', '😌', '🤓',
// //       '😛', '😜', '😠', '😇', '😷', '😈', '👻', '😺', '😸', '😹', '😻', '😼', '😽', '🙀', '🙈',
// //       '🙉', '🙊', '👼', '👮', '🕵', '💂', '👳', '🎅', '👸', '👰', '👲', '🙍', '🙇', '🚶', '🏃', '💃',
// //       '⛷', '🏂', '🏌', '🏄', '🚣', '🏊', '⛹', '🏋', '🚴', '👫', '💪', '👈', '👉', '👉', '👆', '🖕',
// //       '👇', '🖖', '🤘', '🖐', '👌', '👍', '👎', '✊', '👊', '👏', '🙌', '🙏', '🐵', '🐶', '🐇', '🐥',
// //       '🐸', '🐌', '🐛', '🐜', '🐝', '🍉', '🍄', '🍔', '🍤', '🍨', '🍪', '🎂', '🍰', '🍾', '🍷', '🍸',
// //       '🍺', '🌍', '🚑', '⏰', '🌙', '🌝', '🌞', '⭐', '🌟', '🌠', '🌨', '🌩', '⛄', '🔥', '🎄', '🎈',
// //       '🎉', '🎊', '🎁', '🎗', '🏀', '🏈', '🎲', '🔇', '🔈', '📣', '🔔', '🎵', '🎷', '💰', '🖊', '📅',
// //       '✅', '❎', '💯',
// //     ],
// //   },
// //   embedded: {
// //     //icon: embedded,
// //     className: undefined,
// //     component: undefined,
// //     popupClassName: undefined,
// //     defaultSize: {
// //       height: 'auto',
// //       width: 'auto',
// //     },
// //   },
// //   image: {
// //     //icon: image,
// //     className: undefined,
// //     component: undefined,
// //     popupClassName: undefined,
// //     urlEnabled: true,
// //     uploadEnabled: true,
// //     alignmentEnabled: true,
// //     uploadCallback: undefined,
// //     previewImage: false,
// //     inputAccept: 'image/gif,image/jpeg,image/jpg,image/png,image/svg',
// //     alt: { present: false, mandatory: false },
// //     defaultSize: {
// //       height: 'auto',
// //       width: 'auto',
// //     },
// //   },
// //   remove: { icon: eraser, className: undefined, component: undefined },
// //   history: {
// //     inDropdown: false,
// //     className: undefined,
// //     component: undefined,
// //     dropdownClassName: undefined,
// //     options: ['undo', 'redo'],
// //     undo: { icon: undo, className: undefined },
// //     redo: { icon: redo, className: undefined },
// //   },
// }

const styles = {
  card: {
      width: 80 + "vw",
  },
  bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
  },
  title: {
      marginBottom: 16,
      fontSize: 14,
  },
  pos: {
      marginBottom: 12,
  },
}; 

class BlogEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contentState: {
        "blocks": [
            {
              "key": "3v53q",
              "text": " ",
              "type": "atomic",
              "depth": 0,
              "inlineStyleRanges": [],
              "entityRanges": [
                  {
                      "offset": 0,
                      "length": 1,
                      "key": 0
                  }
              ],
              "data": {}
            },
            {
                "key": "637gr",
                "text": "Title Goes Here",
                "type": "header-one",
                "depth": 0,
                "inlineStyleRanges": [],
                "entityRanges": [],
                "data": {}
            },
            {
                "key": "4a92g",
                "text": "SubHeading",
                "type": "header-three",
                "depth": 0,
                "inlineStyleRanges": [
                    {
                        "offset": 0,
                        "length": 10,
                        "style": "BOLD"
                    }
                ],
                "entityRanges": [],
                "data": {}
            },
            {
                "key": "7bm1i",
                "text": "Body Goes Here",
                "type": "unstyled",
                "depth": 0,
                "inlineStyleRanges": [],
                "entityRanges": [],
                "data": {}
            }
        ],
        "entityMap": {
            "0": {
                "type": "IMAGE",
                "mutability": "MUTABLE",
                "data": {
                    "src": "https://www.almanac.com/sites/default/files/birth_month_flowers-primary-1920x1280px_pixabay.jpg",
                    "height": "100px",
                    "width": "100px",
                    "alignment": "none"
                }
            }
        }
      }
    };
  }

  onContentStateChange = (contentState) => {
    this.setState({
      contentState,
    });
    //console.log(contentState);
    //let html = draftToHtml(contentState);
    //this.submitForm(html);
    //console.log(html);
  };

  onSaveClick = () => {
    //const {editorState} = this.state;
    //const content = stateToHTML(contentState, options);
    // Your function to save the content
    // save_my_content(content);
    // const dataInput = (content) => this.title
    //const Jsonhtml = {"content": html}
    //this.submitForm(Jsonhtml);
    //const {contentState} = this.state;
    // this.setState({
    //   contentState,
    // });
    const {contentState} = this.state;
    //console.log(contentState);
    let html = draftToHtml(contentState);
    console.log(html);
    this.submitForm(html);

  }

  submitForm = (content) => {
      //console.log(content);
      // const Jsonfyed = JSON.stringify(content);
      this.props
          .createBlog({
              variables:{
                  content: content
              }
          })
          // .then(({ data }) => {
          // }
          .catch((error => {
              console.log(error);
          }));
  };

  render() {
    const { contentState } = this.state;
    const { classes } = this.props;
    return (
      <div style={{ maxWidth: 100 + "vw"}}>
        <Flexbox flexDirection="column" minHeight="100vh">
            <Flexbox element="header" height="60px">
                <MediaQuery query="(min-width: 1224px)">
                    <SimpleAppBar />
                </MediaQuery>
                <MediaQuery query="(max-width: 1223px)">
                    <SimpleAppBarMobile />
                </MediaQuery>
            </Flexbox>
            <Flexbox display="flex" flexWrap="wrap" justifyContent="space-around" flexGrow={1}>
                <div style={{padding:40}}>
                    <Card className={classes.card} onLoad={this.onclick}>
                        <CardContent>
                          <Editor
                            initialContentState={contentState}
                            toolbarClassName="rdw-storybook-toolbar"
                            wrapperClassName="rdw-storybook-wrapper"
                            editorClassName="rdw-storybook-editor"
                            onContentStateChange={this.onContentStateChange}
                            //readOnly={true}
                            //toolbar={actions}
                          />
                        </CardContent>
                    </Card>    
                </div>
            </Flexbox>
        </Flexbox>
        <Button href="/Blog" variant="fab" style={{ backgroundColor: "#D32F2F", color: "#FFFFFF", position: "fixed", right: 30, bottom: 170, zIndex: 10000 }}>
            <DeleteIcon/>
        </Button>
        <Button variant="fab" style={{ backgroundColor: "#388E3C", color: "#FFFFFF", position: "fixed", right: 30, bottom: 100, zIndex: 10000 }}>
            <PublicIcon/>
        </Button>    
        <Button onClick={this.onSaveClick} variant="fab" style={{ backgroundColor: "rgba(103, 58, 183, 1)", color: "#FFFFFF", position: "fixed", right: 30, bottom: 30, zIndex: 10000 }}>
            <SaveIcon/>
        </Button>    
    </div>
    )
  }
}


BlogEditor.propTypes = {
  classes: PropTypes.object.isRequired,
};

const BlogEditorStyled = withStyles(styles)(BlogEditor);

export default graphql(createBlog, {
  name: "createBlog",
  options: {
      refetchQueries: [
          'blog'
      ]
  }
})(BlogEditorStyled);