import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { withApollo } from 'react-apollo';
import BlogPostFunc from './BlogPostFunc';

// const styles = {
//   card: {
//     maxWidth: 345,
//   },
//   media: {
//     height: 200,
//   },
// };

const blogsQuery = gql `
  query blog {
    blog {
      _id,
      content
    }
  }  
`;

const SimpleMediaCard = ({ blog, loading }) => {
  if (loading) return null;
  return (
    <div>
      {blog.map(blog => (
        <div style={{padding: 20}} key={blog._id}>
            <Card>
              <CardMedia
                image="/static/images/cards/contemplative-reptile.jpg"
                title="Contemplative Reptile"
              />
              <CardContent>
                <Typography gutterBottom variant="headline" component="h2">
                {/* {blog.title} */}
                </Typography>
                <div dangerouslySetInnerHTML={{ __html: blog.content }} />
              </CardContent>
              <CardActions>
                <Button size="small" style={{color: "#D32F2F"}}>
                  Delete
                </Button>
                <Button size="small" color="primary">
                  Learn More
                </Button>
              </CardActions>
            </Card>
        </div>
      ))}
    </div>
  );
}

// function SimpleMediaCard(props) {
//   blog
//   const { classes } = props;
//   return (
    
    
//   );
// }




// SimpleMediaCard.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

// const SimpleMediaCardStyled = withStyles(styles)(SimpleMediaCard);
 
export default graphql(blogsQuery, {
   props: ({data}) =>({ ... data })
 })(withApollo(SimpleMediaCard));
